# shell.nix

let
  pkgs = (import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {});
in
  with pkgs;

  mkShell {
    buildInputs = [
      gnumake
      pandoc
      zig
      nodejs_20
      nodePackages_latest.pnpm
      inotify-tools
      tectonic
    ];
  }
