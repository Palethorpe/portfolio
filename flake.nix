{
  description = "Pandoc static website";

  inputs = {
    pkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    # zig = {
    #   url = "github:mitchellh/zig-overlay";
    #   inputs.nixpkgs.follows = "pkgs";
    # };
    # zls.url = "github:zigtools/zls";
  };

  outputs = { self, pkgs }:
    let
      sys = "x86_64-linux";
    in
      with pkgs.legacyPackages.${sys};
      {
        devShells.${sys}.default = mkShell {
          buildInputs = [
            zig
            zls

            pandoc
            gnumake
            nodejs_20
            nodePackages_latest."@tailwindcss/typography"
            nodePackages_latest.tailwindcss
            inotify-tools
            tectonic
            git
            go
            xxd
          ];
        };
        packages.${sys}.default = stdenv.mkDerivation {
          name = "portfolio";
          src = ./.;
          buildInputs = [
            pandoc
            gnumake
            nodejs_20
            nodePackages_latest."@tailwindcss/typography"
            nodePackages_latest.tailwindcss
          ];
          buildPhase = ''
            mkdir public
            make -j$(nproc)
          '';
          installPhase = ''
            mkdir -p $out/public
            cp public/* $out/public
          '';
        };
      };
}
