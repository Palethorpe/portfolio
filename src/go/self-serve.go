package main

import (
	"flag"
	"fmt"
	"log"
	"mime"
	"os"
	"path/filepath"
	"strings"

	"github.com/valyala/fasthttp"
)

var (
	files = make(map[string][]byte)
	types = make(map[string]string)
)

func main() {
	dir := flag.String("dir", ".", "Directory to serve files from")
	flag.Parse()

	err := loadFiles(*dir)
	if err != nil {
		log.Fatalf("Error loading files: %v", err)
	}

	handler := func(ctx *fasthttp.RequestCtx) {
		path := string(ctx.Path())
		if path == "/" {
			path = "/index.html"
		}

		content, ok := files[path]
		if !ok && strings.HasSuffix(path, ".html") {
			content, ok = files[strings.TrimSuffix(path, ".html")]
		}

		if !ok {
			ctx.Error("File not found", fasthttp.StatusNotFound)
			return
		}

		contentType := types[path]
		if contentType == "" {
			contentType = "application/octet-stream"
		}

		ctx.SetContentType(contentType)
		ctx.SetBody(content)
	}

	addr := "0.0.0.0:8080"
	fmt.Printf("Server is running on http://%s\n", addr)
	log.Fatal(fasthttp.ListenAndServe(addr, handler))
}

func loadFiles(dir string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		content, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		relativePath := strings.TrimPrefix(path, dir)
		relativePath = filepath.ToSlash(relativePath)
		if !strings.HasPrefix(relativePath, "/") {
			relativePath = "/" + relativePath
		}

		files[relativePath] = content

		ext := strings.ToLower(filepath.Ext(path))
		switch ext {
		case ".html":
			types[relativePath] = "text/html"
			files[strings.TrimSuffix(relativePath, ".html")] = content
			types[strings.TrimSuffix(relativePath, ".html")] = "text/html"
		case ".map":
			types[relativePath] = "application/json"
		default:
			types[relativePath] = mime.TypeByExtension(ext)
		}

		return nil
	})
}
