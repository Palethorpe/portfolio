---
title: Software freelancing list
description: A small list of platforms for freelancing as a software developer
---

There are two equally terrible strategies to doing anything significant.

1. Jump straight in head first (active)
2. Wallow in research and talk forever (passive)

I don't recall anyone saying to do (2), but it's often said to be
worse than (1). In my opinion this is because the vast graveyard of
people who do (1) and fail are mostly silent. Meanwhile the successful
1'ers are noisy and visible.

So what do you do? Passively research what you are about to do while
actively probing the water. This is where I am now. Anyway below is a
list of freelance platforms I have compiled for my own purposes.

For the most part I rely on what the companies themselves say and
cross reference it with information from places like Crunchbase,
IndieHackers and Reddit.

# General

Basically they all provide some billing services and whatnot. These
things aren't that interesting so I won't cover them.

# Gun.io

I generally get a good feeling from Gun.io. My perception is that they
would provide me with more value than simply an introduction to
clients.

- Senior only
- In depth vetting process, but no tests
- Charge clients a retainer
- Some VC funding
- Some hand holding and matching
- Fast

::: { .message .is-info }
:::: { .message-body }

Update: Also web developer focused. However after speaking to them I
        think my feeling was correct.

::::
:::

# Lemon.io

I also get a good feeling from these guys. Although they don't seem to
be into the crusty C code market.

- Middle and senior upwards
- Web developer focused
- Accept teams
- Targetted at startups (i.e. lower rates)
- 0% commission (I didn't get to the bottom of how they earn)
- Only seed funding

# Toptal

Definitely the gorilla in the room throwing its weight
around. According to third-party sources they add a margin on how much
the freelancer charges. It doesn't say anywhere how much and I tend to
think this is because it is variable with a high lower bound. I also
think though that if you need them then you need them.

- They have the biggest brand
- Fast
- Opaque
- Make you do a Codility test and fake project
- Only seed funding
- They match you with clients

# Upwork

It is what it is. My brother in-law says that you have to put a lot of
work in to get work, but it is a good platform if you put in the time.

- Free-for-all
- Significant upfront effort
- Freelancer reviews
- Low commission
- Lots of jobs to bid on
- Bidding on jobs requires tokens
- Plus membership available

I initially did not realise they have a token system. It costs money
to buy tokens, but you get some free. This makes spamming costly and
from having hired someone on one of these sites before, I can tell you
this is a good thing.

# Contra

Kind of an odd one, they charge for fancy portfolios not
commission. How long this will last is debatable as they will need a
lot of portfolio customers to pay off their funding (assuming such
things matter again).

- 0% commission
- Free-for-all
- Significant VC funding
- More design focused perhaps

# Codementor.io

- On demand code review and pair programming
- Also freelance projects

# Arc

- Same company as codementor, but more vetting(?)
- Remote jobs and freelance

# Braintrust

OK, this is where I start to lose it. Basically all of the companies
that do more than provide a platform do some obfustication. Most
likely because not all clients and talent are created
equal. Publishing a flat fee would not be wise if some engagements can
and should have a higher fee than others.

Braintrust look a bit like the other vetted platforms. What's really
interesting though is that they are a non-profit which took $23M in VC
funding and then did a $123M initial coin offering.

They charge clients a fee on top of the amount they pay to the
freelancer. So they then say that the freelancer gets to keep 100% of
what they make. This of course assumes that the freelancer would
charge the same on platform and off it.

It gets better, because apparently this fee goes towards buying back
their token. Not to their employees or investors who appear to be
substantial. If you are familiar with blockchain shenanigans then I'm
sure you get the point. If you are not, then perhaps you don't and
that is the point of blockchain.

- Vetting and you can earn tokens for vetting new candidates
- Blockchain
- Small fee
- Blockchain
- You can earn tokens for referals
- Blockchain
- They did work for NASA; its going to the Moooon!

# richiejp.com

Improbable to produce work for now, but it is nearly a free
option. I'm going to do the work regardless of whether I freelance,
create another SaaS or work on an Open Source project.
