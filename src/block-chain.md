---
title: Why I am selling my Crypto
description: In the unlikely event block-chain solves a real problem, no crypto token holders will profit from it. Purchasing tokens is gambling.
---

*This is an article primarily for friends and family; a warning from
someone you know.*

*TLDR; in the unlikely event block-chain solves a real problem, no
crypto token holders will profit from it. Purchasing tokens is
gambling.*

On a basic technical level Bitcoin is awful and has been superseded by
Monero^[Which is probably also old hat by now, but it's a while since
I investigated what criminals on the dark web are up to]. The only
practical use of Bitcoin was criminal activity. This is past tense
because the authorities are now aware of how easily traceable it is. I
have not owned any significant amount of Bitcoin for a long time. The
electricity and silicon costs alone are reason enough for me to dump
it. It's depressing even having to talk about it.

So this article is not about Bitcoin. I have opinions about Bitcoin,
but I'm an expert on moving bytes around, checking bits are in the
corrected order, that sort of thing. I don't know about whatever it is
that keeps bitcoin going. For that you should consult Nassim Taleb's
Bitcoin "Black paper".

This isn't about other pure crypto "currencies", like Monero or
Reserve, either. Perhaps these can function as a *medium of exchange*
or a *store of value*.^[In my opinion, reserve currencies require PoM
(Proof of Military) and it is no coincidence the dollar is backed by
the world's best military]. It is doubtful, but that is not something
I can talk about authoritatively. So this is about block-chains and
their associated tokens e.g. Ethereum, Solana etc.

The narrative is that these are going to revolutionise the world. Much
like the internet has done and even the movement of compute resources
to the *Cloud*. This is going to start in places like Africa for
things like tracking the distribution of people and goods.

Of course, people living in Europe have difficulty verifying anything
that happens in Africa. This is due to basic physical and cultural
barriers. It is difficult to know that something is really happening
in a remote part of the world. Even a place such as New York which has
an excellent internet connection and is easily accessible.

There is an obvious disconnect between sensory data being recorded
digitally and what is actually taking place. Not to mention how one
chooses to interpret complex data. I have not the faintest idea how
block-chain can help with these issues. It doesn't sit at the
interface between the physical and the virtual.

It's not clear if block-chain can help with *recording* data, it's a
distributed data *storage* and *transformation* technology. There is a
whole load of hardware and software in between the physical world and
the block-chain.

Recording data requires a physical interaction with the world followed
by a series of transmissions. Eventually the data is stored somewhere,
which could be a block-chain, a combination of block-chain and other
technologies or just a regular old database with some cryptography
thrown in.

Transmission may require a number of hops, with transformations
applied to the data along the way. This could be because the source
data is too large and needs reducing. Perhaps some kind of electronic
tampering can be detected using cryptography. Perhaps multiple parties
need to vote on something to agree it happened. This is not only
plausible, it already exists and it doesn't require a block-chain.

Block-chains, by their very nature, require a large network to
operate. Otherwise they are not distributed. Public block-chains
require the public internet. By the time data reaches the public
internet, there has been plenty of opportunity to tamper with it. If
the local networks are not to be trusted for some reason. Then the
data must already be protected cryptographically.

Still it's possible there is a place in the world for slower
semi-validated distributed databases with a fee structure built
in. Which is essentially the nature of block-chains. I haven't ruled
it out as a possibility. Nor do I need to, I can sit back and wait for
the technology to mature, not giving it too much time or attention.

::: { .message .is-info }
:::: { .message-body }

Since writing this and thinking about what [causes projects to
fail](ways-to-help-your-project-fail). It's becoming increasingly
clear that block-chain breaks all the rules of robust
engineering. This still doesn't rule out that a use will be found for
it. However it makes the technology and any project based on it,
incredibly fragile.

::::
:::

What I can rule out, is that owning crypto tokens is an investment. At
best it is speculative gambling. Something I am not above. However I
much prefer to speculate on things which have some kind of connection
to reality. This is just a pure game of betting on which crypto coin
has the best narrative to sell or will be pumped by a big player next.

What I don't see is a reason why the end users of a block-chain would
pay sky high network fees instead of starting a new network. Public
block-chains are necessarily limited in the volume of transactions
they can perform. Validation and synchronisation are necessarily
expensive operations in terms of computer resources. Therefor a high
fee has to be paid for each transaction to pay back those who bought
into the network as well as those who provided the computational
resources.

Spinning up some "validator", compute and storage nodes is
relatively trivial. Just select some cloud vendors and fire up some
preconfigured virtual machine images.

All of the software is Open Source and made so that independent
parties can run it. Third party services, such as oracles, that use
the block-chain are made to be interoperable. There is no lock-in to a
particular block-chain or even to block-chain at all.

If you don't trust cloud vendors, you can buy some preconfigured boxes
and stick them wherever. If you can't afford that then you also can't
afford to pay token owners enough to justify massive valuations. If
you are not technical enough, you are not technical enough to protect
your *private keys*. You will have to trust a third party somewhere
down the line.

To cut a long story short, I can't see a logical reason why a group of
end users, people who are *getting shit done*, are going to pay for
some tokens. If they need that technology, they will find the cheapest
way of obtaining it. Paying back token holders is a useless expense
and is easily avoidable.

There are no technical "network effects", that would ensure one block
chain network becomes dominant. It's relatively easy for users to
access multiple block-chains at once. It's easy to transfer tokens
between chains and even convert "smart contracts", from one chain to
another.

Possibly one chain could become dominant if network fees stayed the
same as the number of users increased. In such a case though there has
to be losers and it's going to be passive token holders.

Some of these arguments could be falsified by an instance of a block
chain application with stable profitability. However I can not find a
single non-circular block-chain project with significant end users
paying for the service.

Neither can I see a reason why I would personally use block-chain or
web3 except as a free promotion. Even then I wouldn't trust it with
sensitive data. I'd ensure I can run my infra on regular cloud or bare
metal as well.

This pains me, because I absolutely love the idea of "comoditizing"
cloud services and giving AWS a kick in the teeth. I really wanted
web3 to be viable, but looking at it pragmatically, it appears to be
unnecessary complication.

Ironically block-chains are transparent, which I really like. You can
peer in at everything happening through a block-chain explorer. Look
at the programs being executed and what they do. Usually this is just
maintaining the network itself or some Dex/NFT/game nonsense. All
stuff that circles back around to block-chain.

At this point the only way to invest in block-chain is to learn the
tech and try to use it. I started on that path and found a small
amount of tech mixed with huge amounts of bullshit. There is always
some bullshit with new tech, but the ratio here is not good.

The environment appears very conducive to making money as a freelance
software developer. The problem is the level of horse shit crosses
over from exaggeration and enthusiasm into outright fraud. Operating
in such an environment while maintaining some sense of ethics, is more
than my little heart could bear.

Frankly we would not be talking about block-chain if it weren't for
the speculative mania around Cryptos. Friends and family don't ask me
about conflict-free-replicated-data-types or cryptographically
verifiable computations in general.

NFTs are the purest form of bollocks imaginable. There is nothing
technically preventing anyone from copying them. Their value is a pure
social construct. Frankly not too dissimilar to physical paintings and
other types of art. There is nothing concrete for me to say about
them.

However they probably show what block-chain really is, a social
construct. The manifestation of belief in technology. NFTs are
obviously bollocks and proud of it. This is fine by me. Where other
types of block-chain token fall foul is they claim to be something
other than art.

The closer I look at such claims, the more they appear to be
chimerical. Fantasy beasts made up of parts that don't make sense. I
can't rule out in absolute terms that some of this technology will
come to fruition. Occasionally "putting wings on a horse and seeing if
it flies", works.

The probability though is small and the probability that token holders
will reap the profits is zero. I have made quite a profit on Crypto
myself, but my interest is rapidly waning as I continue to fail to
find anything concrete at its base.

So I'm selling up and sharing my findings as fair warning. Be careful
giving any of those fuckers your money.

