---
title: "Richie's Techbits newsletter: Issue 2: Erasure codes to Entropy, Apple SLAP and FLOP, ZOHO and n8n"
description: This week I stumbled into entropy which always leaves me uncertain.
---

This week I stumbled into entropy which always leaves me uncertain.

# In this weeks issue

- Siamese codes, Huffman codes and an introduction to information theory from NASA
- SLAP and FLOP; Apple's turn to suffer speculation attacks
- ZOHO CRM has it's own language
- Tools: n8n

# Entropy

While trying to understand a library for [streaming forward erasure correction codes](https://github.com/catid/siamese)
I was once again reminded how close probability theory lurks in the background of computer engineering.

I have a collection of computer science text books and I was thinking to myself "which one of those did I see error correction codes in?".
It turned out that none of them had a satisifactory explanation of these, however I do have a large book on probability theory that goes
into a lot of detail.

It has the catchy title "Probability, Random Variables and Stochastic
Processes" and was written by Athanasios Papoulis and S. Unnikrishna Pillai. It
contains a section on information theory which in my niave estimation boils
down to calculating how many bits of *information* a particular combination of
*data* bits represents.

If we receive a byte of data (`8 = 2^3` bits) and each data bit has an equally
likely probability of being 1 or 0 then we have 8 bits of information. Put
another way if we have 256 symbols (`2^8`) and each symbol has an equal
likelyhood of occuring then we have `log2(256) = 8` bits of information.

However let's say that the byte of information we receive is almost always
equal to the ASCII code for either `{` (`01111011`) or `[` (`01011011`) as is
the case when transmitting the first byte of a JSON object or array. I say
"almost always" to allow for corruption. In this case, although we get 8 bits
of data, we are not receiving 8 bits of information. It's more like 3 bits and
actually if I calculate the "self entropy" with a 2% chance of corruption then
it comes out as approximately `1.28` bits of information.

Ironically we get a higher information value the higher the chance of
corruption. With completely random noise having the highest value. Although I
should note that if the corruption had a strong pattern to it then it's
information or entropy value could be lower. You can also choose to merge all
symbols except `{` or `[` into a single "corrupted" symbol in which case the
maximum entropy will occur when there is a `1/3` chance of corruption and will
decrease thereafter.

If this makes no sense to you then I came across [this gem written by Jon C. Freeman and published by NASA](https://ntrs.nasa.gov/api/citations/19970009858/downloads/19970009858.pdf).
Unlike the text book I mentioned, it is written with engineers in mind and it
shows. [Huffman codes](https://www.linkedin.com/feed/update/urn:li:activity:7292384863393849344/),
errasure codes and so on all follow from information theory which in turn
is an offshoot of probability theory. At least that is one way of
viewing it.

# SLAP and FLOP

I missed this somehow, but Apple got a turn at having to deal with
[hardware vulnerabilities in their CPUs](https://bsky.app/profile/lukaszolejnik.bsky.social/post/3lgurdyknu72o).
As usual it involves speculation of one form or another which CPUs do
for performance.

Apparently data needs to be in the same address space for these techniques to
work (don't quote me on that), but one expects a real Safari exploit to
require more than one bug.

Again probability theory crops up here because speculation attacks usually
rely on timing certain events and finding a statistically significant
timing difference between when speculation has happened or not.

# ZOHO CRM has its scripting language

On my hunt to integrate my calendar app with more services (I think Google's
verification team have put me in the slow lane) I discovered ZOHO Deluge.

ZOHO is a vast CRM and office suite plus a wild range of other stuff including a
serverless platform and nocode tools. For some reason they have a programming language
that appears to be not based on anything I particularly recognise.

I couldn't see a way to shoe horn my app into their app store, so decided to go
try it on with Webflow instead which I'll leave for another time. It's worth noting though
that ZOHO is big enough to have its own ecosystem and specialists. Not close to the
same level as Google Workspaces, but I could see significant activity on Upwork.

# Tools

Somehow ended with a very short list this week.

- https://github.com/n8n-io/n8n - An ~Open~ Fair source Zapier? Definitely one for the AI agent party.

