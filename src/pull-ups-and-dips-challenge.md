---
title: 100 pull-ups and 100 dips per day challenge
description: Attempting to get to 100 reps per day
---

*If you got here from my index page, then please don't be alarmed, but this is
not an article about software development.*

![One hundred dips man](onedipman.jpg)

Guru Anaerobic, who's book, *Gang Fit*, I have been reading. Said that he did
6x10 pull-ups, 4x10 of something else (curls?) and 10x10 dips per day, every
day, for a month. This resulted in a serious improvement to strength and
muscle mass.

::: { .message .is-warning }
:::: { .message-body }

Update!

For reasons I don't understand this article gets some traffic. So I
have to say that the above person, along with similar individuals,
have a track record of encouraging people until serious injury.

This is just an anecdotal report of something I tried. I was in
reasonable shape when I started it.

::::
:::

From everything I have read and experienced over the last 2 years, this
kind of thing shouldn't work. If nothing else, it should be suboptimal,
resulting in over-training and injury. So I decided to try it.

My version of the challenge is to do 100 pull ups and 100 dips per day for a
month (or until my wife gives birth). The only problem is that I can't do that
many dips (unless I spread it over a few hours), never mind pull ups. So I did
as many full pull ups and dips as I could (in sets of 10 reps ideally) until
failure. Then moved to a secondary exercise.

For dips, which I can do more of, I switch out of diamond press ups, then when
I can't do anymore of those, to regular press ups, then when that fails I put
my knees down, until I am basically just flopping around on the floor like a
fish out of water.

For pull ups I started switching out to various types of curl, but I hate
curls, so eventually replaced these with bent over rows which I find more
satisfying.

# Log

What follows is a very rough log of the exercise. I didn't record my rest
periods or when I only did 2 sets of 5 reps instead of 1 set of 10 reps for
pull ups. Also I did all of the pull ups on my door frame on some days and
some on the monkey bars or football goal others. It is much easier on the
monkey frame, which doesn't a climber style finger grip to stop it from
digging into my fingers.

So this comes down quite a lot to perception, but the numbers maybe help a
bit. Below are approximately the number of sets I did of 10 reps.

Day	Pull-ups	Rows	Dips	Press-ups	Notes
---	---------	-----	-----	---------	-----
1	3			6		5		3			Didn't actually do rows, just curls
2	2			6		5		5			Experimenting with secondary exercises, pulled back muscle
3	3			7		5		5			Also did some hill sprints, used park frame, slept 12 hours that night
4	1			9		8		2			Sudden improvement is dips (also did them with less rest between sets!), switched fully to rows, bicep hurts
5	4			6		7		3			Fatigued, less dips, but cleaner and less rest between sets, also did a few sprints, visible increase in muscle mass
6	7			3		10		0			Maybe more rest between sets
7	4			6		10		0			Forearms hurting in a novel way. Wife complains that I have ketone breath despite the fact I have been lax with intermittent fasting and carb intake.
8	6			4		10		0			Used park frame for pull-ups and did some sprints too. Did 2x20 dip sets and 6x10. Still had to do some of the pull-ups as 2 sets of 5, but feel like there is some improvement.
9	5			5		10		0			Increased row weight a bit, helps get the rows to a more similar difficulty to the pull-ups. Feel far less like I am over-training.
10	6			4		10		0			Not the cleanest sets, but less rest time.
11	5			5		10		0			I feel like my pull-ups are increasing in range and the reps are better quality.
12	5			5		10		0			Also did sprints, quite tired.
13	7			3		10		0
14	5			5		10		0
15	3			7		10		0			Also did sprints and jogging. It is hard to tell if I am going backwards or forwards with pull-ups.
16	2			8		10		0			Hung over, bit of a shambles
17	5			5		10		0			Sprints again and some extras. Had some pretensions about doing a 48 hour fast, but the thought of going to bed without eating was too much this time.
18	1			9		10		0			Woke up early so went for a run as well. Starting to feel fatigue again. Need to back off a bit, wife is near her due date. Did another 20 hour fast.
19	1			9		10		0			Ate lunch today and still backing off pull-ups, want to try and recover a bit.
20	2			8		10		0			Woke up too early and couldn't go back to sleep, feel pretty knackered. 20 is a nice round number so I think I'm done!

# Injury, fatigue and performance

Pull-ups are always a high risk move for me. Usually when I am trying to eek
out one last rep is when something goes in my back and this happened on day
two. However I carried on, it hurts when I sneeze, but somehow not so much
during actually exercise. Conspicuous by its absence is the tendon strain I
used to get in my arms when rock climbing. I suppose that plain two-handed
pull-ups are putting less strain on my joints compared to many climbing moves
where you often have most of your weight on one outstretched arm.

Fatigue is also present, but disappears during actual exercise. The effects of
*overtraining* are clearly present, but qualitatively my performance improved
in a noticeable way by day 4. At least for dips, pull-ups were harder to
judge. Around day 8-9 I noticed that the feeling of fatigue was significantly
reduced along with aches and pains.

Towards the end I was in a bit of a state some days. This could have been
because some sessions were just harder due to extra bits I threw in. However
it also seemed to be building up. I could have continued, but it seemed like
the wrong time to be pushing myself physically.

# "Theorizing"

Thinking about how our ancestors might have lived, the fact this works makes
sense to me. When times were very good or very bad, they may have had to hunt
or fight every day for a prolonged period. There is a fair amount of material
suggesting just 1-2 hours of exercise a week, with plenty of rest in between
sessions, is optimal. However I think in the wild it is likely you would get
days or weeks of intense activity followed by a period of rest.

When large animals were passing by, eating grass and farting, hunters would
have to track them for hours at a moderate pace, followed by an intense burst
of sprinting and spear throwing. Then they would have to haul anything they
couldn't eat on the spot back to camp, home or at least away from any large
cats or dogs. If it was too heavy to move, then maybe they would have to fight
off anything which also wanted to eat it.

Of course this is just one possible scenario of many. It is just a thought
exercise. I think it is reasonable to think about such things because it gives
you some grounding outside of circular health and fitness metrics. The optimal
diet and exercise for health and fitness depends on your environment and your
environment provides your diet and exercise.

*Assuming* that some combination of habitats from the bulk of our evolution
are optimal (excluding the occasional extreme stresses which result in death)
is reasonable to avoid epistemological issues. If you accept that we *evolved*
in a given set of environments, then we probably perform best in a permutation
of those. It is not guaranteed by evolution and our evolutionary environment
is non-extant. However it provides a counter to arbitrarily chosen metrics
defining health and fitness.

