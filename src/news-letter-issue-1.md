---
title: "Richie's Techbits newsletter: Issue 1: Deepseek, Apps Script and more"
description: I didn't like DeepSeek, I liked Apps Script even though its rubbish, tools, planes, finance and security
---

I'm starting a weekly news letter to note down and share interesting things I have found
in technology and business. Comments and corrections welcome.

# What's in this issue

- I tried DeepSeek and didn't like it
- Google Apps script is wierd and clunky but I like it
- Tools: ngrok, ryelang, scrap/scratch, Bun, ZML, Cilium
- Other: Boom Supersonic, DeepSeek impact on financial markets, Zseano's bug bounty methodology

# Richie's opinion

## I tried DeepSeek and didn't like it

Well at least I thought I did...

As soon as the DeepSeek R1 drama found its way into my X feed I wanted to try it on Groq as an alternative to Llamma 3.x. Groq produces tokens very quickly and cheaply and the idea of having a model as good as Claude on there is very attractive one.

Unfortunately they don't have the full DeepSeek R1 on there, the full model is a staggering 671B parameters which even if each parameter is only 16-bit that is over a terabyte. It's also a mixture of experts architecture (MoE). Possibly Groq's specialised hardware has difficulty with both.

What Groq did put up though was a Llama 3.3 70b variant which I found to be worse than the original Llama. It used more tokens because it does some initial "thinking out loud", then hallucinated more and produced more verbiage.

Admittedly I tested it on Google Apps script which appears to be quite difficult for all of LLMs, including Google's. Claude 3.5 did quite well, but oddly hallucinated more than Llama 3.3 when I started to copy and paste Google's documentation into the prompt. More on Google Apps script later.

I haven't actually been able to try the full R1 model, DeepSeek won't allow me to sign up.

I suspect that once developers creating LLM "wrapper" products get more chance to evaluate DeepSeek, there will be some backlash as people find it doesn't perform well for their niche. The time it takes to have an impact could be much longer than some have suggested.

## Google Apps Script is a hotbed of activity

So from the hottest thing in tech to one of the most boring. Second only to VBA in terms of eye watering cringe. However I quite like it, I shouldn't do, I'm the type of software developer who is drawn towards new tech and OSS like a moth to a flame, but I still like it.

I'm using it to create a Google Workspace's plugin for my [availability calendar web](https://dobu.uk) app. Google have essentially forced me into this if I want to get on the Workspaces add-on directory.

There is nothing particularly good about Apps Script, it's just JavaScript that runs in a sandbox on Google's infra and can automate or extend most of Google's products. It has its own weird little IDE which I don't use, instead opting to write TypeScript and upload the results using Clasp. The documentation seems oddly difficult to navigate and grok. Finally the scripts take a long time to execute even if they are not fetching any data.

It all feels a little broken down and neglected, like no one at Google really cares that much about it. Certainly like no one cares too much about add-on developers having a smooth experience in the cloud console.

Despite all that it provides a pretty effective "duct tape", that brings all of the Google services together. What's more it is clearly very well used by many businesses who rely heavily on Google's office suite for their operations. There is a hose pipe of requests coming into Upwork every day for Apps Script automations.

# tools

- https://ngrok.com/: Host web apps or APIs locally even if you don't have a static IP
- https://ryelang.org/: Caught my eye on HN because it has a library for spread sheets
- scrap and scratch https://bsky.app/profile/raysan5.bsky.social/post/3lgo7bpgskk2n
    - scratch https://scratch.mit.edu/about
- https://bun.sh/blog/bun-v1.2 released: It's like nodejs, but faster and has more built in. I find it
  interesting that a whole web app could be built just using Bun's builtin features
- https://zml.ai: Run AI models on any hardware (except my Intel Arc of course)
- https://github.com/cilium/ebpf: Write Go that runs inside the Linux kernel

# other

- Soon it may once again be possible for rich people to do a round trip from New York to London
  in one day. Opening up the exciting possibility of being able to go shopping in London and be
  back for dinner in New York. There are more serious uses for fast transport as well which I'll
  to the reader to figure out
  https://x.com/boomaero/status/1884320653840392587?t=2X-b_6CRZnn8Gq8os2Ojbg&s=19

- "High Beta Vs. Low Volatility Large Caps: Largest Divergence Since GFC"
  https://x.com/priceactionlab/status/1884174855869972689?t=E9Hr114ENn_Ij7Z0B5bLmw&s=19
  DeepSeek's release had some interesting consequences for the financial markets and should
  serve as a warning.

- https://www.bugbountyhunter.com/methodology/zseanos-methodology.pdf introduction to how to
  go about finding security vulnerabilities in web applications and getting paid for it.

