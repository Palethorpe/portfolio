/*Copyright (c) 2022 Richard Palethorpe <io@richiejp.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

#define _GNU_SOURCE

#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

static void serve_file(const int sk, const int public_dir)
{
	char recv_buf[BUFSIZ];
	char head_buf[BUFSIZ];
	const size_t buf_len = BUFSIZ - 1;
	char path_buf[256];
	char *file_path;
	ssize_t recv, sent;
	size_t recv_total = 0, sent_total = 0;
	int body_fd;

	while (1) {
		recv = read(sk,
			    recv_buf + recv_total,
			    buf_len - recv_total);

		if (recv < 0) {
			perror("[-] read");
			return;
		}

		if (!recv) {
			dprintf(STDERR_FILENO,
				"[-] End of data before header was received\n");
			return;
		}

		recv_total += recv;
		recv_buf[recv_total] = 0;

		if (strstr(recv_buf, "\r\n\r\n"))
			break;

		if (recv_total >= buf_len) {
			dprintf(STDERR_FILENO,
				"Exceeded buffer reading header\n");
			return;
		}
	}

	printf("[*] <<<\n%s\n", recv_buf);
	if (!sscanf(recv_buf, "GET %250s HTTP/1.1", path_buf)) {
		dprintf(STDERR_FILENO,
			"[-] 'GET <file_path> HTTP/1.1' not matched in:\n %s",
			recv_buf);
	}

	if (!strcmp("/", path_buf)) {
		strcpy(path_buf, "index.html");
		file_path = path_buf;
	} else if (path_buf[0] == '/') {
		file_path = path_buf + 1;
	}

	printf("[*] Opening %s", file_path);
	body_fd = openat(public_dir, file_path, O_RDONLY);

	if (body_fd < 0 && errno == ENOENT) {
		strcpy(file_path + strlen(file_path), ".html");
		body_fd = openat(public_dir, file_path, O_RDONLY);
		printf(" failed trying with .html");
	}
	printf("\n");

	if (body_fd < 0) {
		perror("[-] openat");
		return;
	}

	const char *const http_head =
		"HTTP/1.1 200 OK\r\n"
		"Connection: close\r\n"
		"Content-Type: %s\r\n"
		"Content-Length: %lu\r\n"
		"\r\n";
	const char *mime = "text/html";
	if (strstr(file_path, ".css"))
		mime = "text/css";
	if (strstr(file_path, ".map"))
		mime = "application/json";
	if (strstr(file_path, ".svg"))
		mime = "image/svg+xml";
	if (strstr(file_path, ".jpg"))
		mime = "image/jpg";
	if (strstr(file_path, ".png"))
		mime = "image/png";

	struct stat body_stat;
	if (fstat(body_fd, &body_stat)) {
		perror("[-] fstat");
		goto close_body;
	}
	sprintf(head_buf, http_head, mime, body_stat.st_size);
	printf("[*] >>>\n%s", head_buf);

	while (sent_total < strlen(http_head)) {
		sent = write(sk, head_buf + sent_total, strlen(head_buf));

		if (sent < 0) {
			perror("[-] write");
			goto close_body;
		}

		sent_total += sent;
	}

	do {
		sent = sendfile(sk, body_fd, NULL, body_stat.st_size);

		if (sent < 0) {
			perror("[-] sendfile");
			goto close_body;
		}

		sent_total += sent;
	} while (sent > 0);

close_body:
	close(body_fd);
}

int main(const int argc, const char *const argv[])
{
	const pid_t orig_parent = getppid();
	const struct sockaddr_in self_addr = {
		.sin_family = AF_INET,
		.sin_port = htons(9000),
		.sin_addr = {
			htonl(INADDR_LOOPBACK)
		}
	};
	const int listen_sk = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr client_addr;
	socklen_t addr_len;

	if (argc < 2) {
		dprintf(STDERR_FILENO,
			"usage: %s <dir to serve files from>\n",
			argv[0]);
		return 1;
	}
	const int public_dir = open(argv[1], O_PATH);

	if (bind(listen_sk, (struct sockaddr *)&self_addr, sizeof(self_addr))) {
		perror("bind");
		return 1;
	}

	if (listen(listen_sk, 8)) {
		perror("listen");
		return 1;
	}

	printf("[+] Listening; press Ctrl-C to exit...\n");

	while (orig_parent == getppid()) {
		const int sk = accept(listen_sk, &client_addr, &addr_len);

		if (sk < 0) {
			perror("[-] accept");
			break;
		}

		printf("[+] Accepted Connection\n");

		serve_file(sk, public_dir);
		close(sk);
	}

	return 0;
}
