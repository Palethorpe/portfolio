const std = @import("std");
const Type = std.builtin.Type;
const mem = std.mem;
const expect = std.testing.expect;
const expectEqual = std.testing.expectEqual;
const print = std.debug.print;

const UnpackError = error{
    IntTooBig,
};

/// Get the unsigned type big enough to count the bits in T.  Needed
/// because Zig constrains the right hand side of a shift to an
/// integer only big enough to perform a full shift. Which is only u3
/// for u8 (for e.g.).
///
/// Meanwhile I don't know a way to specify this type other than to
/// construct it like this.
fn ShiftSize(comptime T: type) type {
    const ShiftInt = Type{
        .Int = .{
            .signedness = std.builtin.Signedness.unsigned,
            .bits = comptime std.math.log2_int(u16, @bitSizeOf(T)),
        },
    };

    return @Type(ShiftInt);
}

fn decodeInt(comptime T: type, comptime n: u3, buf: *[]const u8) !T {
    const prefix = (1 << n) - 1;
    var b = buf.*[0];
    var i: T = b & prefix;

    if (i < prefix) {
        buf.* = buf.*[1..];
        return i;
    }

    var j: ShiftSize(T) = 1;
    while ((j - 1) * 7 < @bitSizeOf(T)) : (j += 1) {
        b = buf.*[j];

        i += @as(T, (b & 0x7f)) << (7 * (j - 1));

        if (b < 0x80)
            break;
    } else {
        return UnpackError.IntTooBig;
    }

    buf.* = buf.*[j + 1 ..];
    return i;
}

fn encodeInt(comptime n: u3, val: anytype, buf: []u8) !usize {
    const vtype = @TypeOf(val);
    const prefix: u8 = (1 << n) - 1;

    if (val < prefix) {
        buf[0] &= ~prefix;
        buf[0] |= @truncate(u8, val);
        return 1;
    }

    buf[0] |= prefix;

    var i: vtype = val - prefix;
    var k: usize = 1;

    while (true) {
        buf[k] = 0x80 | @truncate(u8, i);

        i >>= 7;
        k += 1;

        if (i == 0)
            break;
    }

    buf[k - 1] &= 0x7f;

    return k;
}

test "decode prefix int" {
    var buf = [_]u8{ 0x6f, 0x7f, 0x7f, 0x01 };
    var slice: []const u8 = buf[0..];

    try expect(try decodeInt(u8, 7, &slice) == 0x6f);
    try expect(slice[0] == 0x7f);
    try expect(slice.len == 3);

    slice = buf[0..];
    buf[0] = 0x7f;
    try expect(try decodeInt(u16, 7, &slice) == 0xfe);
    try expect(slice.len == 2);

    const buf2 = [_]u8{ 0x1f, 0xff, 0xff, 0x01 };
    slice = buf2[0..];
    try expectEqual(@as(u32, 0x1f + 0x7f + (0x7f << 7) + (0x01 << 14)), try decodeInt(u32, 5, &slice));

    const buf3 = [_]u8{0x05};
    slice = buf3[0..];
    try expectEqual(@as(u32, 0x05), try decodeInt(u32, 4, &slice));
}

test "encode prefix int" {
    var buf = [_]u8{0xff} ** 8;

    try expectEqual(@as(usize, 2), try encodeInt(7, @as(u8, 0x7f), &buf));
    try expect(buf[0] == 0xff);

    buf[0] = 0x00;
    try expect(try encodeInt(7, @as(u16, 0x6f), &buf) == 1);
    try expect(buf[0] == 0x6f);

    buf[0] = 0xe0;
    try expect(try encodeInt(5, @as(u32, 0xff), &buf) == 3);
    try expect(buf[0] == 0xff);
    try expectEqual(@as(u8, 0xe0), buf[1]);
    try expect(buf[2] == 0x01);
    try expect(buf[3] == 0xff);

    buf[0] = 0x00;
    try expect(try encodeInt(5, @as(u8, 0xff), &buf) == 3);
    try expect(buf[0] == 0x1f);
    try expect(buf[1] == 0xe0);
    try expect(buf[2] == 0x01);
    try expect(buf[3] == 0xff);

    buf[0] = 0x00;
    try expect(try encodeInt(7, @as(u8, 0x7f), &buf) == 2);
    try expectEqual(@as(u8, 0x7f), buf[0]);
    try expectEqual(@as(u8, 0x00), buf[1]);

    buf[0] = 0xff;
    try expect(try encodeInt(4, @as(u8, 0x05), &buf) == 1);
    try expectEqual(@as(u8, 0xf5), buf[0]);
}

test "encode decode prefix int" {
    var buf = [_]u8{0xff} ** 16;
    var i: u32 = 2;
    var j: u32 = 3;
    var k: u32 = 5;

    while (k < ~@as(u32, 0) - (j + i)) : ({
        const n = i + j;

        i = j;
        j = k;
        k = n;
    }) {
        const used = try encodeInt(4, @as(u32, k), &buf);
        var slice: []const u8 = buf[0..used];
        try expectEqual(k, try decodeInt(u32, 4, &slice));
    }

    const max_int = ~@as(u64, 0);
    const used = try encodeInt(7, max_int, &buf);
    var slice: []const u8 = buf[0..used];
    try expectEqual(max_int, try decodeInt(u64, 7, &slice));
}
