// Copyright (c) 2025
// Richiejp Ltd. <io@richiejp.com>
//
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// THIS SOFTWARE IS PROVIDED BY [Name of Organization] “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL [Name of Organisation] BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

override stride: u32 = 16;

struct Params {
  width: u32,
  height: u32,
  rule: u32,
  reversible: u32,
  zoom: u32,
  panx: f32,
  pany: f32,
};

@group(0) @binding(0) var<storage, read_write> cells: array<u32>;
@group(0) @binding(1) var<uniform> params: Params;

// Take bit n of m and if it is 1 return all 1s, if 0 return all 0s
@must_use fn bit_to_max(m: u32, n: u32) -> u32 {
  let z: u32 = 0;

  return ((m >> n) & 1) * ~z;
}

@compute @workgroup_size(stride) fn cmp(
  @builtin(global_invocation_id) id: vec3u
) {
  let rule = params.rule;
  let i = id.x;
  let cols = stride;
  let rows = arrayLength(&cells) / cols;

  // Wrap the u32 bitfields; if we are on the first or last index
  let left_i = select(i - 1, cols - 1, i == 0);
  let right_i = select(i + 1, 0, i == cols - 1);

  // The cells before the current ones, used for reversible automata
  var prev: u32 = 0;

  for (var j: u32 = 1; j < rows; j++) {
    let row_off = (j - 1) * cols;
    let center = cells[row_off + i];
    // move the cell-bits on the left and right into the center
    // the bits at the edge are taken from the nieghboring bitfields
    let left  = (center >> 1) | (cells[row_off + left_i ] << 31);
    let right = (center << 1) | (cells[row_off + right_i] >> 31);

    var result: u32 = 0;

    // for each of the 8 3-bit patterns...
    for (var k: u32 = 0; k < 8; k++) {
      // is the cell (de)activated for this pattern?
      let on = bit_to_max(rule, k);
      // check if the left, center and right cell-bits match the pattern
      // it's useful to remember that we are working on 32 cells at once
      let l = ~(bit_to_max(k, 2) ^ left);
      let c = ~(bit_to_max(k, 1) ^ center);
      let r = ~(bit_to_max(k, 0) ^ right);

      // set cell-bits to active if pattern is active and left,
      // right and center cell-bits all matched
      result |= l & c & r & on;
    }
    // for reversible automata...
    result ^= prev;

    cells[j * cols + i] = result;
    workgroupBarrier();

    prev = select(center, 0, params.reversible == 0);
  }
}

struct Verts {
  @builtin(position) pos: vec4f,
  @location(0) color: vec4f,
};

@vertex fn vs(@builtin(vertex_index) i : u32) -> Verts {
  let pos = array(
    vec2f(-1, 1),
    vec2f(-1,-1),
    vec2f( 1,-1),
    vec2f(-1, 1),
    vec2f( 1, 1),
    vec2f( 1,-1),
  );
  let col = array(
    vec4f(0.1, 0.5, 1, 1),
    vec4f(0.1, 1, 0.5, 1),
    vec4f(0.1, 0.5, 1, 1),
  );

  return Verts(vec4f(pos[i], 0.0, 1.0), col[i % 3]);
}

@fragment fn fs(verts: Verts) -> @location(0) vec4f {
  let fields = stride;
  let cols = (32 * fields) / params.zoom;
  let col_width = f32(params.height) / f32(cols);
  let rows = (arrayLength(&cells) / fields) / params.zoom;
  let row_height = f32(params.width) / f32(rows);
  let i_off = f32((32 * fields) - cols) * params.pany;
  let j_off = f32((arrayLength(&cells) / fields) - rows) * params.panx;

  // get the cell's column index
  let i = u32(floor(verts.pos.y / col_width + i_off));
  // get the field index; i / 32
  let f = i >> 5;
  // get the bit index; i % 32
  let b = 31 - (i & 31);
  let j = u32(floor(verts.pos.x / row_height + j_off));
  let a = 0.2 + 0.8 * f32(((cells[j * fields + f]) >> b) & 1);

  return a * verts.color;
}

