// Copyright (c) 2025
// Richiejp Ltd. <io@richiejp.com>
//
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// THIS SOFTWARE IS PROVIDED BY [Name of Organization] “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL [Name of Organisation] BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

var params = {
  rule: 105,
  seed: 114322622,
  reversible: 1,
  zoom: 2,
  deviceType: 'mobile',
  panx: 0.5,
  pany: 0.5,
};
const wgslReq = fetch('/automata.wgsl').then(r => r.text());

main();

function render(ctx, renderPassDesc, pipe, { dev, bindGroup }) {
  renderPassDesc.colorAttachments[0].view = ctx.getCurrentTexture().createView();
  const enc = dev.createCommandEncoder({ label: 'autoamata compute enc' });
  const pass = enc.beginRenderPass(renderPassDesc);

  pass.setPipeline(pipe);
  pass.setBindGroup(0, bindGroup);
  pass.draw(6);
  pass.end();

  const cmdBuf = enc.finish();
  dev.queue.submit([cmdBuf]);
}

async function compute(pipe, { dev, bindGroup }) {
  const enc = dev.createCommandEncoder({ label: 'automata compute enc' });
  const pass = enc.beginComputePass({ label: 'automata compute pass' });

  pass.setPipeline(pipe);
  pass.setBindGroup(0, bindGroup);
  pass.dispatchWorkgroups(1);
  pass.end();

  const cmdBuf = enc.finish();
  dev.queue.submit([cmdBuf]);
}

function initComponents() {
  const zoomInput = document.getElementById('zoom');
  const zoomValueSpan = document.getElementById('zoom-value');

  zoomInput.addEventListener('input', (e) => {
    zoomValueSpan.textContent = e.target.value;
    params.zoom = parseInt(e.target.value);
  });

  const ruleInput = document.getElementById('rule');
  const ruleValueSpan = document.getElementById('rule-value');

  ruleInput.addEventListener('input', (e) => {
    ruleValueSpan.textContent = e.target.value;
    params.rule = parseInt(e.target.value);
  });

  const seedInput = document.getElementById('seed');
  const seedValueSpan = document.getElementById('seed-value');

  seedInput.addEventListener('input', (e) => {
    seedValueSpan.textContent = e.target.value;
    params.seed = parseInt(e.target.value);
  });

  const reversibleCheckbox = document.getElementById('reversible');

  reversibleCheckbox.addEventListener('change', (e) => {
    params.reversible = e.target.checked ? 1 : 0;
  });

  const deviceTypeCombo = document.getElementById('device-type');

  deviceTypeCombo.addEventListener('change', (e) => {
    params.deviceType = e.target.value;
  })

  const exportImgBtn = document.getElementById('export-img');

  exportImgBtn.addEventListener('click', () => {
    const canvas = document.getElementById('gpu-canvas');

    canvas.toBlob((blob) => {
      const a = document.createElement('a');

      a.href = URL.createObjectURL(blob);
      a.download = `${params.rule}${params.reversible ? 'r' : ''}-${params.deviceType}.png`;
      a.click();
    }, 'image/png');
  })

  const panxInput = document.getElementById('pan-x');
  const panxValue = document.getElementById('pan-x-value');
  const panyInput = document.getElementById('pan-y');
  const panyValue = document.getElementById('pan-y-value');

  panxInput.addEventListener('input', (e) => {
    params.panx = parseFloat(e.target.value);
    panxValue.textContent = e.target.value;
  });
  panyInput.addEventListener('input', (e) => {
    params.pany = parseFloat(e.target.value);
    panyValue.textContent = e.target.value;
  });
}

async function runPipelines() {
  const canvas = document.querySelector("#gpu-canvas");
  const adapter = await navigator.gpu?.requestAdapter();
  const dev = await adapter?.requestDevice();
  const ctx = canvas.getContext("webgpu");
  const presentationFormat = navigator.gpu.getPreferredCanvasFormat();

  if (!dev) {
    console.error("Unable to initialise WebGPU!")
    return;
  }

  var stride;
  if (params.deviceType === 'desktop') {
    stride = 32;
    canvas.width = 1820;
  } else {
    stride = 16;
    canvas.width = 412;
  }
  canvas.height = stride * 32;

  ctx.configure({
    device: dev,
    format: presentationFormat,
  });

  const module = dev.createShaderModule({
    label: 'automata shader',
    code: await wgslReq,
  });

  const computePipe = dev.createComputePipeline({
    label: 'automata compute pipeline',
    layout: 'auto',
    compute: {
      constants: { stride },
      module,
    },
  });

  const renderPipe = dev.createRenderPipeline({
    label: 'automata render pipeline',
    layout: 'auto',
    vertex: {
      module,
    },
    fragment: {
      constants: { stride },
      module,
      targets: [{ format: presentationFormat }],
    },
  });

  const renderPassDesc = {
    label: 'automata renderPass',
    colorAttachments: [
      {
        // view:
        clearValue: [0.8, 0.2, 0.2, 1],
        loadOp: 'clear',
        storeOp: 'store',
      },
    ],
  };

  const canvasTexture = ctx.getCurrentTexture();
  if (canvasTexture.height % 32 > 0)
    console.warn("Canvas texture height is not divisible by the integer width (32-bits)");

  const cells = new Uint32Array(canvasTexture.width * stride);
  const parray = [
    canvasTexture.width,
    canvasTexture.height,
    params.rule,
    params.reversible,
    params.zoom,
    params.panx,
    params.pany,
  ];
  const paramABuf = new ArrayBuffer(4 * parray.length);
  const uparams = new Uint32Array(paramABuf);
  const fparams = new Float32Array(paramABuf);

  const cellBuf = dev.createBuffer({
    label: "automata out buffer",
    size: cells.byteLength,
    usage: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_DST,
  });
  const paramBuf = dev.createBuffer({
    label: "automata param buffer",
    size: uparams.byteLength,
    usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
  })

  const computeBindGroup = dev.createBindGroup({
    label: 'buffer bindGroup',
    layout: computePipe.getBindGroupLayout(0),
    entries: [
      { binding: 0, resource: { buffer: cellBuf  } },
      { binding: 1, resource: { buffer: paramBuf } },
    ],
  });
  const renderBindGroup = dev.createBindGroup({
    label: 'buffer bindGroup',
    layout: renderPipe.getBindGroupLayout(0),
    entries: [
      { binding: 0, resource: { buffer: cellBuf  } },
      { binding: 1, resource: { buffer: paramBuf } },
    ],
  });
  const lastDeviceType = params.deviceType;

  async function animate() {
    const shared = { dev, cellBuf };

    if (lastDeviceType !== params.deviceType) {
      runPipelines();
      return;
    }

    parray[2] = params.rule;
    parray[3] = params.reversible;
    parray[4] = params.zoom;
    parray[5] = params.panx;
    parray[6] = params.pany;
    uparams.set(parray.slice(0, 5), 0);
    fparams.set(parray.slice(5), 5);

    for (var i = 0; i < stride; i++) {
      if (params.seed < 0)
        cells[i] = params.seed / i;
      else
        cells[i] = 0;
    }
    cells[stride / 2] = params.seed;

    dev.queue.writeBuffer(cellBuf, 0, cells);
    dev.queue.writeBuffer(paramBuf, 0, paramABuf);

    compute(computePipe, { bindGroup: computeBindGroup, ...shared });
    render(ctx, renderPassDesc, renderPipe, { bindGroup: renderBindGroup, ...shared });

    requestAnimationFrame(animate);
  }

  requestAnimationFrame(animate);
}

async function main() {
  initComponents();

  await runPipelines();
}
