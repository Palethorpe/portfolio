FROM scratch

ENV PATH=/bin

ADD ./src/go/portfolio /bin/
ADD ./public /public

ENTRYPOINT [ "portfolio", "-dir", "/public" ]
