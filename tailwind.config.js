/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/*.tmpl"],
  theme: {
    extend: {
      typography: ({ theme }) => ({
        slate: {
          css: {
            '--tw-prose-bullets': theme('colors.slate[400]'),
          },
        },
      }),
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

