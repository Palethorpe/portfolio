inputs = $(wildcard src/*.md)
pages = $(subst src,public,$(inputs:.md=.html))
static = $(subst static,public,$(wildcard static/*))
pdfs = $(subst src,public,$(inputs:.md=.pdf))

all: $(pages) $(static) public/sitemap.xml public/atom.xml public/output.css
pdfs: $(pdfs)

$(static): public/%: static/%
	cp $< $@

$(pages): src/std.tmpl src/bio.tmpl
$(pages): public/%.html: src/%.md
	pandoc --from=markdown+yaml_metadata_block \
		--standalone \
		--css=dobuuk.css --css=output.css \
		--section-divs \
		--template=src/std.tmpl \
		--shift-heading-level-by=1 \
		--toc --toc-depth 2 \
		--highlight-style=zenburn \
		--syntax-definition=res/syntax/zig.xml \
		$< > $@

$(pdfs): public/%.pdf: src/%.md
	pandoc --from=markdown+yaml_metadata_block \
		--toc --toc-depth 3 \
		--highlight-style=zenburn \
		--syntax-definition=res/syntax/zig.xml \
		--pdf-engine=tectonic \
		-V 'mainfont:montserrat' \
		-V 'documentclass:report' \
		-V 'titlegraphic:src/logo.svg' \
		-V 'author:Richard Palethorpe' \
		--output $@ \
		$<

public/output.css: $(pages) src/input.css
	tailwindcss -i ./src/input.css -o ./public/output.css

page-names := $(subst public/,,$(pages))
page-names := $(subst .html,,$(page-names))
page-names := $(filter-out index,$(page-names))
public/sitemap.xml: $(pages)
	script/sitemap.sh $(page-names) > $@

page-meta = $(subst src,meta,$(inputs:.md=.yml))
$(page-meta): meta/%.yml: src/%.md
	script/meta.sh $< > $@

public/atom.xml: $(inputs) $(page-meta) src/atom.tmpl script/atom.sh
	script/atom.sh $(filter-out services,$(page-names)) > $@

indexnow-key := $(shell xxd -p -l 32 -c 32 /dev/urandom)
indexnow:
	echo -n ${indexnow-key} > ./public/${indexnow-key}.txt

build/self-serve: src/self-serve.c
	gcc -fanalyzer -fno-omit-frame-pointer -fsanitize=address,undefined -Wall -Wextra $< -o $@

src/go/portfolio: src/go/self-serve.go src/go/go.mod src/go/go.sum
	cd ./src/go && CGO_ENABLED=0 go build -ldflags "-s -w" .

docker: src/go/portfolio all
	docker build -t portfolio .

fly: src/go/portfolio all fly.toml
	fly deploy

watch:
	while true; do \
		$(MAKE) -j$(nproc); \
		inotifywait --exclude ./public -qre close_write .; \
	done

clean:
	rm public/*

.PHONY: $(page-meta) fly docker indexnow
