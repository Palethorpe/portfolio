#!/bin/sh -eu

echo updated: $(git log -1 --pretty="format:%cI" $1)
echo published: $(git log --pretty="format:%cI" $1 | tail -n 1)

