#!/bin/sh -eu

cat <<EOF
<?xml version="1.0" encoding="UTF-8" ?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Richard Palethorpe's software engineering articles</title>
  <id>https://richiejp.com</id>
  <updated>$(date -Iseconds -u)</updated>
  <subtitle>Systems engineering (C, Zig, Kernel, Databases), Full-Stack (TypeScript, Svelte, Go), Indiehacking</subtitle>
  <author>
    <name>Richard Palethorpe</name>
    <email>io@richiejp.com</email>
    <uri>https://richiejp.com</uri>
  </author>
  <link href="https://richiejp.com/atom.feed.xml" rel="self" />
  <icon>/favicon.svg</icon>
  <logo>/logo.svg</logo>
  <rights> © $(date +%Y) Richard Palethorpe</rights>
EOF

for pname in $*; do
        pandoc --from=markdown+yaml_metadata_block \
               --to=html4 \
               --template=src/atom.tmpl \
               -M link=https://richiejp.com/$pname \
               --metadata-file=meta/$pname.yml \
               src/$pname.md
done

cat <<EOF
</feed>
EOF
